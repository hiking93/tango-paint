﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PaintButtonController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

	private bool m_isButtonPressed = false;

	public bool isPressed() {
		return m_isButtonPressed;
	}

	public void OnPointerDown(PointerEventData eventData) {
		m_isButtonPressed = true;
	}

	public void OnPointerUp(PointerEventData eventData) {
		m_isButtonPressed = false;
	}
}
