﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawingSpaceController : MonoBehaviour {

	public void clearChildren() {
		foreach (Transform child in transform) {
			GameObject.Destroy(child.gameObject);
		}
	}

	void Start () {
	}

	void Update () {
	}
}
