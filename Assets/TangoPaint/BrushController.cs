﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrushController : MonoBehaviour {

	private GameObject m_drawingSpace;
	private PaintButtonController m_paintButton;
	private Material m_material;

	private float m_brushSize = 0.01f;
	private float m_minDistance;
	private Vector3 m_lastPosition;
	private bool m_strokeEnded = true;

	public void setBrushSize (float size) {
		m_brushSize = size;
		m_minDistance = size / 3f;
		this.gameObject.transform.localScale = new Vector3(size, size, size);
	}

	public void setBrushColor (Color color) {
		m_material = new Material(Resources.Load("PaintMaterial", typeof(Material)) as Material);
		m_material.color = color;
		this.gameObject.GetComponent<Renderer>().material = m_material;
	}

	void Start () {
		m_drawingSpace = GameObject.Find("/DrawingSpace");
		m_paintButton = GameObject.Find("PaintButton").GetComponent<PaintButtonController>();

		setBrushColor(Color.white);
		setBrushSize(m_brushSize);
	}

	void Update () {
		Vector3 currentPosition = this.gameObject.transform.position;
		float distance = Vector3.Distance(m_lastPosition, currentPosition);

		if (m_paintButton.isPressed()) {
			if (m_strokeEnded) {
				GameObject startSphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
				startSphere.transform.parent = m_drawingSpace.transform;
				startSphere.transform.position = m_lastPosition;
				startSphere.transform.localScale = new Vector3(m_brushSize, m_brushSize, m_brushSize);
				startSphere.GetComponent<Renderer>().material = m_material;
				m_strokeEnded = false;
			}
			if (distance > m_minDistance) {
				Vector3 pos = Vector3.Lerp(m_lastPosition, currentPosition, 0.5f);

				GameObject cylinder = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
				cylinder.transform.parent = m_drawingSpace.transform;
				cylinder.transform.position = pos;
				cylinder.transform.localScale = new Vector3(m_brushSize, distance / 2, m_brushSize);
				cylinder.transform.up = currentPosition - m_lastPosition;
				cylinder.GetComponent<Renderer>().material = m_material;

				GameObject endSphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
				endSphere.transform.parent = m_drawingSpace.transform;
				endSphere.transform.position = currentPosition;
				endSphere.transform.localScale = new Vector3(m_brushSize, m_brushSize, m_brushSize);
				endSphere.GetComponent<Renderer>().material = m_material;

				m_lastPosition = currentPosition;
			}
		} else {
			m_lastPosition = currentPosition;
			m_strokeEnded = true;
		}
	}
}
